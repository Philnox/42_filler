###
### For run a HUMAN vs BOT game
cd ./lets_play_bot_vs_human
make
./run.sh

###
### For run a BOT vs BOT game
cd ./lets_play_bot_vs_bot
make
./make_games.sh